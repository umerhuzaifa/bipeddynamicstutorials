

% How to derive the equations of motion for the example model -- a single
% leg composed of two links. The lengths are lth, lsh, and the two masses
% are mth and ms. The distance of mass mth in the second link from one end
% is rth and distance of mass ms in the first link is rs.

% Umer Huzaifa
% February 24, 2021

clear all
close all
clc

dbstop if error

% The system is an exoskeleton leg with only two degrees of freedom
% provided by cable-driven mechanism above the wearer.

syms q1 q2 real
syms th1 th2 real
syms dq1 dq2 real


% Model parameters

syms rth rsh lth lsh real
syms mth ms g real

q = [q1, q2]'; 
dq = [dq1, dq2]';


p_th = rot2(q1) * [0; -rth]; %rth * [sin(q1); -cos(q1)];
p_kn = rot2(q1) * [0; -lth]; % [sin(q1); -cos(q1)];
p_sh = p_kn + rot2(q1+q2) * [0; -rsh]; %rs * [-sin(q2); -cos(q2)]

v_sh = jacobian(p_sh, q) * dq;
v_th = jacobian(p_th, q) * dq;

KE = simplify(0.5 * ms * v_sh'*v_sh + 0.5 * mth * v_th'* v_th);

PE = simplify(ms * p_sh(2) * g + mth * p_th(2) * g);

D = jacobian(jacobian(KE, dq).', dq);

N=max(size(q));
syms C
for k=1:N
    for j=1:N
        C(k,j)=0*g;
        for i=1:N
            C(k,j)=C(k,j)+1/2*(diff(D(k,j),q(i))+...
                diff(D(k,i),q(j))-...
                diff(D(i,j),q(k)))*dq(i);
        end
    end
end

G=simplify(jacobian(PE,q).');

% The system currently considers the right hand side of the equation of
% motion to be zero i.e. no motor connected to it.

save('example_model.mat')



% Preparing for writing all the matrices in .m files

%%% Data preparation for saving in files

% Preparing the data and the matrices for storage in file

params = {'mth', mth;...
    'ms', ms;...
    'lth', lth;...
    'ls', ls;...
    'rth', rth;...
    'rsh', rsh;...
    'g', g;
    };

% Writing all the system parameters into some special form of vectors
data = {} ;
for j=1:length(params)
    data(j,1) = params(j,1) ;
    data(j,2) = {['params(' num2str(j) ')']} ;
end
m_list_params = data;
%%%%

% mapping functions to relate all the variables to their index positions in
% q, dq, and ddq
[~, m_list_q] = gen_c_m_lists(q, 'q');
[~, m_list_dq] = gen_c_m_lists(dq, 'dq') ;

% x here is equivalent to z
[~, m_list_x] = gen_c_m_lists([q; dq], 'x') ;


% Now, writing short quick one-liners for getting D, C, G and some other
% matrices by writing functions to get these matrices

 
m_output_dir = pwd;   % makes sure that new functions are stored in the same folder
write_fcn_m([m_output_dir '\fcn_D.m'],{'q', 'params'},[m_list_q; m_list_params],{D,'D'});
write_fcn_m([m_output_dir '\fcn_C.m'],{'q','dq', 'params'},[m_list_q;m_list_dq;m_list_params],{C,'C'});
write_fcn_m([m_output_dir '\fcn_G.m'],{'q', 'params'},[m_list_q;m_list_params],{G,'G'});

