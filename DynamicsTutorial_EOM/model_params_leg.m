function [mth, ms, lth, ls, rth, rsh, g] = model_params_leg
    
    % Masses of the cuffs and supporting hardware on-board
    
    mth = 0.6; % kg
    ms = 0.54; % kg
   
   %% Location of the cuffs, and thigh link length from their respective origins
   
   lth = 0.60; % m from the hip joint. Taken for 50th percentile men from https://www.ele.uri.edu/faculty/vetter/BME207/anthropometric-data.pdf  
   ls  = 0.5; % m from the knee. Taken for 50th percentile men from https://www.ele.uri.edu/faculty/vetter/BME207/anthropometric-data.pdf
   
   % Design parameter of the attachment point for thigh and shank cuffs
   rsh = 0.25; % m from the knee. Choosing as a design parameter
   rth = 0.3; % from the hip joint. Choosing as a design parameter 
   
   %% Location of cable connection points on the links
   % Attachment angles for the cables 
%    
%    theta_thf = 30 * (pi/180);
%    theta_thb = 30 * (pi/180);   
%    theta_shf =  30 * (pi/180);
%    theta_shb =  30 * (pi/180);
%    

   % Other simulation parameters
    
   g = 9.8; % m/s/s -- gravitational acceleration
   

   params = [mth, ms, lth, ls, rth, rsh, g];
