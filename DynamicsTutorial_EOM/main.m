% Main code to set up the differential equation and provide the initial
% conditions


% Umer Huzaifa
% February 24, 2021


clear all % clear all the workspace variables
close all % closes all the open figures
clc        % clears the command window


t0 = 0; % initial time for integration
tf = 10; % final time for integration

% providing the initial conditions for the differential equation
z0 = [pi/4; -pi/4; 0; 0]; 

[t, z] = ode45(@model, [t0, tf], z0);


figure
qt = z(:,1:2);
dqt = z(:, 3:4);
plot(t, qt)
legend('q_1','q_2')

figure
plot(t, dqt)
legend('\dot q_1', '\dot q_2')