function [C] = fcn_C(q,dq,params)

C = zeros(2,2);

  C(1,1)=-dq(2)*params(3)*params(2)*params(6)*sin(q(2));
  C(1,2)=- dq(1)*params(3)*params(2)*params(6)*sin(q(2)) - dq(2)*params(3)*params(2)*params(6)*sin(q(2));
  C(2,1)=dq(1)*params(3)*params(2)*params(6)*sin(q(2));
  C(2,2)=0;

 