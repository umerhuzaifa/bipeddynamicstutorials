function [G] = fcn_G(q,params)

G = zeros(2,1);

  G(1,1)=params(7)*params(2)*(params(6)*sin(q(1) + q(2)) + params(3)*sin(q(1))) + params(7)*...
         params(1)*params(5)*sin(q(1));
  G(2,1)=params(7)*params(2)*params(6)*sin(q(1) + q(2));

 