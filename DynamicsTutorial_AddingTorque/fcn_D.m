function [D] = fcn_D(q,params)

D = zeros(2,2);

  D(1,1)=params(3)^2*params(2) + params(2)*params(6)^2 + params(1)*params(5)^2 + 2*params(3)*...
         params(2)*params(6)*cos(q(2));
  D(1,2)=params(2)*params(6)^2 + params(3)*params(2)*params(6)*cos(q(2));
  D(2,1)=params(2)*params(6)^2 + params(3)*params(2)*params(6)*cos(q(2));
  D(2,2)=params(2)*params(6)^2;

 