% this function contains the modeling equation parameters
% D, C, and G matrices

% input: current time (t), current value of variable (z)

% output: derivative of variable (dz)

% Umer huzaifa
% February 24, 2021


function dz = model(t, z)

% z = [q; dq];
q = z(1:2);  dq = z(3:4);

% Recall that z2 in the notes is the second half of z vector
z2 = z(3:4);

q1 = q(1); q2 = q(2);
dq1 = dq(1); dq2 = dq(2);

params = model_params_leg;


% D = [ ms*lth^2 + 2* ms* cos(q2) * lth*rsh + ms * rsh^2 + mth*rth^2, lth*ms*rsh*cos(q1 - q2) + ms*rsh*rsh;...
%     lth*ms*rsh*cos(q1 - q2) + ms*rsh*rsh, ms*rsh^2];

D = fcn_D(q, params);

% C = [-dq2*lth*ms*rsh*sin(q2), -lth*ms*rsh*sin(q2)*(dq1 + dq2);dq1*lth*ms*rsh*sin(q2),0];

C = fcn_C(q, dq, params);

% G = [g*ms*(rsh*sin(q1 + q2) + lth*sin(q1)) + g*mth*rth*sin(q1); g*ms*rsh*sin(q1 + q2)];

G = fcn_G(q, params);

tau = zeros(2,1);
% tau = ones(2,1);  % when both the motors are arbitrarily kept on -- instability
tau = C*z2+G;  % when tau is desired to cancel the other terms completely -- no movement in the system, joint velocities dz =0
tau = C*z2 + G - D*ones(2,1); % when tau is canceling and more -- joint angles are quad changing


dz = [z2; D\(tau - C * z2 - G)];   % write out this equation after plugging in Gamma to picture the different cases of motor action


end

